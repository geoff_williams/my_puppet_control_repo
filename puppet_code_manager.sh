#!/bin/bash
#
# Deploy code to a Puppet Enterprise master using Puppet Code Manager using curl
# to trigger its REST API.
#
# Usage
# -----
# Invoke the script with no arguments, it will use shell variables to obtain
# the necessary authentication tokens and hostnames
#
# Environment Varaibles
# ---------------------
#
# `PE_RBAC_TOKEN`
# MANDATORY:  A valid authentication token for the Puppet Enterprise RBAC API
# see https://docs.puppet.com/pe/latest/rbac_token_auth.html#generating-a-token-for-use-by-a-service
# for instructions on how to obtain one
#
# `PE_PUPPET_SERVER_CERTNAME`
# MANDATORY:  The certname belonging to the Puppet Master.  Usually this is the
# same as the hostname we need to use to talk to the machine, however, in cloud
# and multi-homed environments, it can be different.
#
# `PE_CA_CERT_PEM`
# MANDATORY unless insecure mode forced:  The contents of the CA Certificate on
# the Puppet Enterprise master.  This is the contents of the file at
# /etc/puppetlabs/puppet/ssl/certs/ca.pem
#
# `PE_PUPPET_SERVER_ADDRESS`
# OPTIONAL:  Your Puppet Enterprise master's internal certificate name MUST
# match the hostname requested with curl.  You may need this option if you are
# using a public cloud service that allocates both public and private DNS names
# for hosts if you haven't employed the pe_install::puppet_master_dnsaltnames
# option during installation (see: https://docs.puppet.com/pe/latest/install_pe_conf_param.html#additional-parameters-for-monolithic-and-split-installations).  If used, this option specifies the DNS
# name we should use to talk to the puppet master while still using the
# hostname supplied in PE_PUPPET_SERVER_CERTNAME.  This is equivalent to adding
# an entry to /etc/hosts but is less invasive.
#
# `FORCE_INSECURE_CONNECTION`
# OPTIONAL:  Force an insecure connection to the puppet master.  Not recommended
#
# `DEBUG_DEPLOY`
# OPTIONAL:  Run BASH in debug mode

# errors
set -e

# fail right side of pipe if left side fails
set -o pipefail


if [ "$DEBUG_DEPLOY" ] ; then
  echo "Entering debug mode..."
  set -x
fi

CODE_MANAGER_PORT=8170
CODE_MANAGER_ENDPOINT="/code-manager/v1/deploys"
CA_CERT="/tmp/ca.cert"

if [ -z "$PE_RBAC_TOKEN" ] ; then
  echo "Please set PE_RBAC_TOKEN to the contents of a valid Puppet Enterprise RBAC token"
  exit 1
fi
if [ -z "$PE_PUPPET_SERVER_CERTNAME" ] ; then
  echo "Please set PE_PUPPET_SERVER_CERTNAME to the name matching the your Puppet Master's certname.  You can find this by running `puppet config print certname` on the master"
  exit 1
fi
if [ -z "$PE_CA_CERT_PEM" ] ; then
  echo "Please set PE_CA_CERT_PEM to the contents of /etc/puppetlabs/puppet/ssl/certs/ca.pem on your puppet master to verify the certificate chain"
  if [ -z "$FORCE_INSECURE_CONNECTION" ] ; then
    exit 1
  else
    echo "WARNING:  Forcing insecure SSL connection to the Puppet Master, as you have requested"
    CA_OPT="-k"
  fi
else
  # newlines will be flattened to spaces so we need to fixup our pem file
  echo "$PE_CA_CERT_PEM" | awk 'BEGIN{RS=" " ; print "-----BEGIN CERTIFICATE-----"} !/-----/ {print} END{print "-----END CERTIFICATE-----"}' > $CA_CERT
  CA_OPT="--cacert ${CA_CERT}"

  if [ "$PE_PUPPET_SERVER_ADDRESS" ] ; then
    # curl REQUIRES we convert the address/hostname to an IP address
set -e
    PE_PUPPET_SERVER_IP=$(getent ahosts $PE_PUPPET_SERVER_ADDRESS | awk '/STREAM/ { print $1 }')
    CA_OPT="${CA_OPT} --resolve ${PE_PUPPET_SERVER_CERTNAME}:${CODE_MANAGER_PORT}:${PE_PUPPET_SERVER_IP}"
  fi
fi

# undefined variables
set -u

TARGET="https://${PE_PUPPET_SERVER_CERTNAME}:${CODE_MANAGER_PORT}${CODE_MANAGER_ENDPOINT}"
PAYLOAD='{"deploy-all": true}'
echo "Attempt POST to service URI: ${TARGET}"
OUTPUT=$(curl ${CA_OPT} -X POST -H "X-Authentication: ${PE_RBAC_TOKEN}" -H 'Content-Type: application/json' "$TARGET" -d "$PAYLOAD")

if [ $(echo "$OUTPUT" | grep '"status":"queued"') ] ; then
  echo "*** code queued for deployment on ${PE_PUPPET_SERVER_CERTNAME} ***"
else
  echo "Error deploying code to ${PE_PUPPET_SERVER_CERTNAME}:"
  echo "$OUTPUT"
  exit 1
fi
